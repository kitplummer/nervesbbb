defmodule NervesBbb.Environment do

  use GenServer
  require Logger


  def start_link(default) do
    GenServer.start_link(__MODULE__, default)
  end

  def init(args) do
    Logger.debug("Starting environment")

    {:ok, bme680_reference} = Bme680.start_link(i2c_device_number: 2, i2c_address: 0x77)
    spawn(fn -> poll_forever(bme680_reference) end)

    {:ok, args}
  end


  def poll_forever(bme680_reference) do
    measurement = Bme680.measure(bme680_reference)
    Logger.debug(inspect(measurement))

    :timer.sleep(1000)
    poll_forever(bme680_reference)
  end

end
