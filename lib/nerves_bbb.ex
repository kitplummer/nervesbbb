defmodule NervesBbb do
  @moduledoc """
  Documentation for NervesBbb.
  """

  @doc """
  Hello world.

  ## Examples

      iex> NervesBbb.hello
      :world

  """
  def hello do
    :world
  end
end
