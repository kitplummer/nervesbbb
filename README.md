# NervesBbb

BeagleBone Black with MikroBus

## Slot 1
OLED B
Using oled library. For some reason the display doesn't match the coordinates. See configuration in ./config/config.exs
- Top Left:     95, 1
- Top Right:    32, 1
- Bottom Left:  32, 39
- Bottom Right: 95, 39
### Pins
- Reset GPIO 30
- D/C GPIO 50
- SPI Bus 1 Select 0 = /dev/spidev2.0

## Slot 2
Environmental Sensor

## Slot 3
LoRa
### Pins
- Reset GPIO 26
- UART 1 = /dev/ttyS1 baud = 57600

## Slot 4


# Usage
## SPI
Have to include the SPI device overlay for SPI to work. The only way to do this in the project seems to be to add it to the fwup.conf file. Copied the fwup.conf from the BBB project to ./config/ and overwriting it in ./config/config.exs

- uboot_setenv(uboot-env, "uboot_overlay_addr0", "/lib/firmware/BB-SPIDEV0-00A0.dtbo")                                     
- uboot_setenv(uboot-env, "uboot_overlay_addr1", "/lib/firmware/BB-SPIDEV1-00A0.dtbo")
## UART
Same thing with SPI where it needs to be enabled with a device overlay. Mikrobus slots 2 and 3 use UART1 which is device ttyS1
- uboot_setenv(uboot-env, "uboot_overlay_addr2", "/lib/firmware/BB-UART1-00A0.dtbo")
## Wi-Fi
Set environment variables `WIFI_SSID` and `WIFI_PSK` to configure it to connect to that Wi-Fi network on boot.
## SSH
Storing some keys in ./ssh/ to not require using the local user's keys. Copy contents of ssh to ~/.ssh/ This includes public and private keys as well as a config file to tell it what key to use for nerves.local. Annoyingly there's no way to tell the upload.sh script what keys to use since it just calls ssh.
## Install
* `mix deps.get`
* `mix firmware`
* `mix firmware.burn` or `mix firmware.gen.script & ./upload.sh`
